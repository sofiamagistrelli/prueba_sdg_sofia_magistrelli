import VueRouter from 'vue-router'
import Podcasts from '../views/podcasts/Podcasts';

const routes = [
  {path: '/', component: Podcasts, name: 'home'},

  {path: '*', redirectTo: '/'}
];

export default new VueRouter({
  routes,
  mode: 'history'
});
