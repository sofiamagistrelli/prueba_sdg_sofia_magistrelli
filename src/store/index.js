import Vue from 'vue'
import Vuex from 'vuex'
import PodcastsModule from './modules/Podcasts';

Vue.use(Vuex);

const state = {
  loading: false
};

const mutations = {
  toggleLoading(state) {
    state.loading = !state.loading;
  }
};

const getters = {
  loading: state => state.loading
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  modules: {
    podcasts: PodcastsModule
  }
});
