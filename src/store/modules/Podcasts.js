import axios from "axios";

// initial state
const state = {
  all: [],
  filtered: []
};

// getters
const getters = {
  getFilteredPodcasts: state => {
    return state.filtered
  }
};

// actions
const actions = {
  getAllPodcasts({commit, state}) {
    if (state.all.length === 0) {
      commit('toggleLoading', null, {root: true});
      axios.get('https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json').then(result => {
        commit('toggleLoading', null, {root: true});
        commit('setPodcasts', result.data.feed.entry);
        commit('setFiltered', result.data.feed.entry)
      }).catch(error => {
        console.error(error);
        commit('toggleLoading', null, {root: true});
        commit('setPodcasts', []);
      });
    }
  },
  search({commit}, payload) {
    const filtered = state.all.filter((podcast) => {
      return podcast['im:name'].label.toUpperCase().includes(payload.queryString.toUpperCase())
        || podcast['im:artist'].label.toUpperCase().includes(payload.queryString.toUpperCase())
    });
    commit('setFiltered', filtered);
  }
};

// mutations
const mutations = {
  setPodcasts(state, podcasts) {
    state.all = podcasts;
  },
  setFiltered(state, podcasts) {
    state.filtered = podcasts;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
