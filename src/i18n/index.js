import Vue from 'vue'
import VueI18n from 'vue-i18n'


Vue.use(VueI18n);

export default new VueI18n({
  locale: 'en',
  messages: {
    en: {
      'podcasts.author': 'Author: {name}',
      'header.title': 'TOP PODCASTER',
      'podcast.detail.description': 'Description',
      'podcast.detail.episodes.title': 'Episodes: {quantity}',
      //'podcast.detail.author': 'By {author}',
      'podcasts.finder.placeholder': 'Filter podcasts...',
      'podcasts.finder.input': 'Filter podcasts...'
    }
  }
});
