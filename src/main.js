import Vue from 'vue'
import App from './App.vue'
import i18n from './i18n'
import store from './store'
import router from './router';
import VueRouter from 'vue-router';
import VueRamda from 'vue-ramda';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueMoment from 'vue-moment'
import JQuery from 'jQuery';

//IMPORTS
Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(VueRamda);
Vue.use(VueMoment);
Vue.use(JQuery);

//BOOTSTRAPING
new Vue({
  el: '#app',
  i18n: i18n,
  store,
  render:
    h => h(App),
  router
});

JQuery(document).ready(function(){

  JQuery(window).scroll(function(){
    if( JQuery(this).scrollTop() > 0 ){
      JQuery('header').addClass('floatHeader');
    } else {
      JQuery('header').removeClass('floatHeader');
    }
  });

  JQuery('a.scroll-link').click(function(e){
    e.preventDefault();
    let id = JQuery(this).attr('href');
    JQuery('body,html').animate({
      scrollTop: JQuery(id).offset().top -20
    }, 750);
  });
});
