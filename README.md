# Podcaster - VUE

> SPA developed with Vuejs to show a kind of podcasts from Itunes API

## Modules Used
* VueJS
* VueI18n
* Vue-ramda
* VueX
* BootstrapVue

## Build Setup
The SPA is developed with Vuejs and NodeJS.
For build the app you need Node.js and Npm.

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run start_dev

# build for production with minification
npm run build
```
